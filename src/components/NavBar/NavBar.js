import React from 'react';
import {NavLink} from 'react-router-dom';
import './NavBar.css';

const NavBar = () => {
   const activeNavItem = {
      color: 'orange',
      textDecoration: 'underline',
   }
   return (
     <div className='NavBar'>
        <h3>Quotes Central</h3>
        <ul className='nav-wrapper'>
           <li>
              <NavLink to={'/'} isActive={(match, location) => {
                 if ((location.pathname.split('/quotes').length === 2) || location.pathname === '/') {
                    if (location.pathname.split('/edit').length === 2){
                       return false;
                    }
                    return true;
                 }
              }} activeStyle={activeNavItem}>Quotes</NavLink>
           </li>
           <li>
              <NavLink to='/add-quote' activeStyle={activeNavItem}>Submit new quote</NavLink>
           </li>
        </ul>
     </div>
   );
};

export default NavBar;