import React from 'react';
import './Quote.css';

const Quote = ({data, onQuoteDelete, onQuoteRedact}) => {

   return (
     <div className="quote">
        <div className="qoute-btns-block">
           <button onClick={onQuoteRedact}>Redact</button>
           <button onClick={onQuoteDelete}>X</button>
        </div>
        <div className='author'>―{data.author}</div>
        <p>"{data.content}"</p>
     </div>
   );
};

export default Quote;

