import React, {useEffect, useState} from 'react';
import {Bars, Preloader} from "react-preloader-icon";
import {QUOTES_URL} from "../../config";
import axios from "axios";
import './ChangeQuote.scss';

const ChangeQuote = ({routeObj, categories}) => {
   const [categoryVal, setCategoryVal] = useState('all');
   const [authorVal, setAuthorVal] = useState('');
   const [quoteVal, setQuoteVal] = useState('');
   const [isRequesting, setIsRequesting] = useState(false);
   const onQuoteSubmit = async (e) => {
      e.preventDefault();
      const createdData = {
         category: categoryVal,
         author: authorVal,
         content: quoteVal,
      };
      setIsRequesting(true);
      if (routeObj.match.params.id) {
         await axios.put(QUOTES_URL + '/' + routeObj.match.params.id + '.json', createdData);
      } else {
         await axios.post(QUOTES_URL + '.json', createdData);
      }
      routeObj.history.push('/');
   };

   useEffect(() => {
      if (routeObj.match.params.id) {
         setIsRequesting(true);
         const requestQuote = async () => {
            const {data} = await axios.get(QUOTES_URL + '/' + routeObj.match.params.id + '.json')
            setAuthorVal(data.author);
            setQuoteVal(data.content);
            setCategoryVal(data.category);
            setIsRequesting(false);
         }
         requestQuote();
      }

   }, [routeObj])


   return (
     <div className="change-quote-block">
        {isRequesting && (
          <div style={
             {
                position: 'absolute',
                left: '0',
                top: '0',
                display: 'flex',
                zIndex: '1000',
                background: 'rgba(239,238,238,0.7)',
                width: '100%',
                height: '100%',
             }}>
             <div style={{margin: 'auto'}}>
                <Preloader
                  use={Bars}
                  size={132}
                  strokeWidth={10}
                  strokeColor="black"
                  duration={1200}
                />
             </div>
          </div>)}
        <form className='change-quote-form' onSubmit={onQuoteSubmit}>
           <div className="label">
              Category
           </div>
           <select value={categoryVal} onChange={(e) => setCategoryVal(e.target.value)}>
              {categories.map((category) => {
                 return <option value={category.path} key={category.id + category.title}>{category.title}</option>;
              })}
           </select>
           <div className="label">
              Author
           </div>
           <input type="text" value={authorVal} onChange={e => setAuthorVal(e.target.value)} required/>
           <div className="label">
              Quote's text
           </div>
           <textarea value={quoteVal} onChange={e => setQuoteVal(e.target.value)} cols="30" rows="10" required/>
           <button className='submit-btn'>Confirm</button>
        </form>
     </div>
   );
};

export default ChangeQuote;