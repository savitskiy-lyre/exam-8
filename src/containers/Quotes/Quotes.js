import React, {useEffect, useState} from 'react';
import {Preloader, Spinning} from "react-preloader-icon";
import {QUOTES_URL} from "../../config";
import axios from "axios";
import Quote from "../../components/Quote/Quote";
import Categories from "../../components/Categories/Categories";
import './Quotes.css';

const Quotes = ({routeObj, categories}) => {

   const [quotesData, setQuotesData] = useState(null);
   const [isRequsting, setIsRequsting] = useState(false);
   const [onDelete, setOnDelete] = useState(false);
   const currentCategory = categories.reduce((gotIt, category) => {
      if (routeObj.location.pathname === '/quotes/' + category.path) {
         gotIt = category['path']['0'].toUpperCase() + category.path.slice(1, category.path.length)
      }
      return gotIt;
   }, 'All')

   const onQuoteDelete = async (id) => {
      setIsRequsting(true);
      await axios.delete(QUOTES_URL + '/' + id + '.json');
      setOnDelete((prev) => {
         return !prev;
      });
   };

   const onQuoteRedact = (id) => {
      routeObj.history.push(QUOTES_URL + '/' + id + '/edit')
   }
   useEffect(() => {
      const requestQuotes = async () => {
         let plusOption = '';
         for (const category of categories) {
            if (QUOTES_URL + '/' + category.path === routeObj.location.pathname) {
               if (routeObj.location.pathname === QUOTES_URL + '/all') {
                  break;
               }
               plusOption += `?orderBy="category"&equalTo="${category.path}"`;
            }
         }
         const {data} = await axios(QUOTES_URL + '.json' + plusOption);
         setIsRequsting(false);
         setQuotesData(data);
      }
      setIsRequsting(true);
      requestQuotes();
   }, [routeObj, categories, onDelete])

   return (
     <div className="quotes-block">
        <Categories categories={categories}/>
        <div className="quotes-wrapper">
           {quotesData && (
             <div>
                <h4>{currentCategory}</h4>
                <div className="quotes-content">
                   {Object.keys(quotesData).map((quote) => {
                      return (
                        <Quote
                          key={quote}
                          categories={categories}
                          data={quotesData[quote]}
                          onQuoteDelete={() => onQuoteDelete(quote)}
                          onQuoteRedact={() => onQuoteRedact(quote)}
                        />)
                   }).reverse()}
                </div>
             </div>
           )}
           {isRequsting && (
             <div style={
                {
                   position: 'absolute',
                   left: '0',
                   top: '0',
                   display: 'flex',
                   zIndex: '1000',
                   background: 'rgba(239,238,238,0.7)',
                   width: '100%',
                   height: '100%',
                }}>
                <div style={{margin: 'auto'}}>
                   <Preloader
                     use={Spinning}
                     size={132}
                     strokeWidth={10}
                     strokeColor="black"
                     duration={1200}
                   />
                </div>
             </div>
           )}
        </div>
     </div>
   );
};
//

export default Quotes;