import './App.css';
import NavBar from "./components/NavBar/NavBar";
import {Route, Switch} from "react-router-dom";
import Quotes from "./containers/Quotes/Quotes";
import {BASE_URL} from "./config";
import axios from 'axios';
import {useMemo} from "react";
import ChangeQuote from "./containers/ChangeQuote/ChangeQuote";

axios.defaults.baseURL = BASE_URL;

const App = () => {
   const categories = useMemo(() => {
      return ([
         {title: 'All', path: 'all', id: Math.random()},
         {title: 'Star Wars', path: 'star-wars', id: Math.random()},
         {title: 'Famous people', path: 'famous-people', id: Math.random()},
         {title: 'Saying', path: 'saying', id: Math.random()},
         {title: 'Humour', path: 'humour', id: Math.random()},
         {title: 'Motivational', path: 'motivational', id: Math.random()},
      ])
   }, []);
   return (
     <div className="App">
        <NavBar/>
        <Switch>
           <Route exact path='/' render={(routeObj) => <Quotes routeObj={routeObj} categories={categories}/>}/>
           <Route path='/quotes/:id/edit' component={(routeObj) => <ChangeQuote routeObj={routeObj} categories={categories}/>}/>
           <Route path='/quotes' render={(routeObj) => <Quotes routeObj={routeObj} categories={categories}/>}/>
           <Route path='/add-quote' render={(routeObj) => <ChangeQuote routeObj={routeObj} categories={categories}/>}/>
        </Switch>
     </div>
   );
};

export default App;
